# RELEASE NOTES: *aptlog*, Friendly display of apt logs (Ubuntu-like system update).

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 1.1.0**:
  - Fix: continue processing even when *history.log* is empty (after a reinitialization done by log package).

**Version 1.0.0**:
  - First version.
