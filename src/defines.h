//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: aptlog
// Friendly display of apt logs (Ubuntu-like system update)
//------------------------------------------------------------------------------
#ifndef DEFINES_H
#define DEFINES_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h>
#include <dirent.h>
#include <stdbool.h>
#include <ctype.h>
#include <signal.h>
#include <sys/stat.h>
//------------------------------------------------------------------------------
// LOCAL HEADER FILES
//------------------------------------------------------------------------------
#include "myalloc.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define MAX_KEYWORD_LEN					20

#define KEY_STARTDATE					"Start-Date"
#define KEY_COMMAND						"Commandline"
#define KEY_REQUESTED					"Requested-By"
#define KEY_INSTALL						"Install"
#define KEY_UPGRADE						"Upgrade"
#define KEY_REMOVE						"Remove"
#define KEY_ENDDATE						"End-Date"

#define ESCAPE_FOREGROUND_BOLDRED		"\033[1;91m"
#define ESCAPE_FOREGROUND_BOLDYELLOW	"\033[1;93m"
#define ESCAPE_FOREGROUND_PINK			"\033[95m"
#define ESCAPE_ALL_ATTRIBUTES_OFF		"\033[0m"
#define ESCAPE_BOLD 					"\033[1m"

#define ERROR(fmt, ...)		do { fprintf(stderr, ESCAPE_FOREGROUND_BOLDRED "ERROR: " fmt "!!!" ESCAPE_ALL_ATTRIBUTES_OFF "\n", __VA_ARGS__); } while (0)
#define INTERRUPT()			do { fprintf(stdout, "\n" ESCAPE_FOREGROUND_BOLDRED "INTERRUPT!!!" ESCAPE_ALL_ATTRIBUTES_OFF "\n"); } while (0)
#define VERBOSE(fmt, ...)	do { if (verboseMode) fprintf(stdout, ESCAPE_FOREGROUND_PINK "[VERBOSE] " fmt ESCAPE_ALL_ATTRIBUTES_OFF "\n", __VA_ARGS__); } while (0)
#define MAX(a,b)			((a) > (b) ? (a) : (b))
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct apt_package {
	char *name;
	char *installed_version;
	char *new_version;
	struct apt_package *next_package;
} apt_package;

typedef struct apt_session {
	char *start_date;
	char *start_time;
	char *command;
	char *requestedby;
	apt_package *installed_packages;
	apt_package *upgraded_packages;
	apt_package *removed_packages;
	char *end_date;
	char *end_time;
	struct apt_session *previous_session;
	struct apt_session *next_session;
} apt_session;
//------------------------------------------------------------------------------
#endif	// DEFINES_H
