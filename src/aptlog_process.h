//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: aptlog
// Friendly display of apt logs (Ubuntu-like system update)
//------------------------------------------------------------------------------
#ifndef PROCESS_H
#define PROCESS_H
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
extern void	processAptLog( char * );
extern void	cleanAptLog( void );
//------------------------------------------------------------------------------
// EXPORTED VARIABLE
//------------------------------------------------------------------------------
extern size_t length_longest_package;
extern size_t length_longest_old_rev;
extern size_t length_longest_new_rev;
//------------------------------------------------------------------------------
#endif	// PROCESS_H
