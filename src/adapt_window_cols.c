//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "defines.h"
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES SPECIFIC TO THIS SOURCE
//------------------------------------------------------------------------------
#include <unistd.h>
#include <termcap.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <X11/Xlib.h>
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern bool verboseMode;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
/*
Returns the parent window of "window" (i.e. the ancestor of window
that is a direct child of the root, or window itself if it is a direct child).
If window is the root window, returns window.
*/
static Window get_toplevel_parent(Display *display, Window window) {
	Window parent;
	Window root;
	Window *children;
	unsigned int num_children;

	while (1) {
		if (0 == XQueryTree(display, window, &root, &parent, &children, &num_children)) {
			ERROR("%s", "cannot get parent window parameter");
			return (Window)-1;
		}
		if (children) XFree(children);
		if (window == root || parent == root) return window;
		window = parent;
	}
}
//------------------------------------------------------------------------------
static Window _get_display_and_window( Display **display ) {
	*display = XOpenDisplay(NULL);
	if (*display == NULL) {
		ERROR("%s", "cannot connect to X server");
		return (Window)-1;
	}
	Window focus;
	int revert;
	XGetInputFocus(*display, &focus, &revert);
	return get_toplevel_parent(*display, focus);
}
//------------------------------------------------------------------------------
static void _get_window_size( Display *display, Window window, int *window_width, int *window_height ) {
	XWindowAttributes window_attributes;
	XGetWindowAttributes(display, window, &window_attributes);
	*window_width = window_attributes.width;
	*window_height = window_attributes.height;
}
//------------------------------------------------------------------------------
static void _set_window_size( Display *display, Window window, unsigned int window_width, unsigned int window_height ) {
	XWindowAttributes window_attributes;
	XGetWindowAttributes(display, window, &window_attributes);
	XResizeWindow(display, window, window_width, window_height);
}
//------------------------------------------------------------------------------
static int _get_terminal_size( int *lines, int *cols ) {
	struct winsize w;
	if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w)) {
		ERROR("%s", "cannot get terminal size");
		return 0;
	}
	*lines = w.ws_row;
	*cols = w.ws_col;
	return 1;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTION
//------------------------------------------------------------------------------
void adapt_window_cols( int number_of_columns_needed) {
	int lines, cols;
	Display *display;

	Window window = _get_display_and_window(&display);
	if (window != (Window) -1) {
		if (_get_terminal_size(&lines, &cols)) {
			if (cols < number_of_columns_needed) {
				VERBOSE("%s", "Enlargement of the window width for better vision");
				int width, height;
				_get_window_size(display, window, &width, &height);
				int nwidth = width + (number_of_columns_needed - cols + 5) * (width / cols);	// 5 is a margin....
				_set_window_size(display, window, (unsigned int) nwidth, (unsigned int) height);
				_get_window_size(display, window, &width, &height);
			}
		}
	}
}
//------------------------------------------------------------------------------
