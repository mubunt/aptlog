//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: aptlog
// Friendly display of apt logs (Ubuntu-like system update)
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "defines.h"
#include "aptlog_process.h"
#include "adapt_window_cols.h"
#include "aptlog_display.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define LINE1(l)		for (size_t _i = 0; _i < l; _i++) fprintf(stdout, "─");  \
						fprintf(stdout, "\n")
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern bool verboseMode;
extern apt_session 	*session;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
void displayStandard( bool firstOnly ) {
	if (isatty(fileno(stdout)))
		adapt_window_cols((int)(length_longest_package + length_longest_old_rev + length_longest_new_rev + 2));
	apt_session *p = session;
	while (p != NULL) {
		fprintf(stdout, ESCAPE_FOREGROUND_BOLDYELLOW "%s - %s / %s. Command: %s" ESCAPE_ALL_ATTRIBUTES_OFF "\n", p->start_date, p->start_time, p->end_time, (p->command == NULL) ? "UNKNOWN" : p->command);
		fprintf(stdout, ESCAPE_FOREGROUND_BOLDYELLOW "%34sRequested by: %s" ESCAPE_ALL_ATTRIBUTES_OFF "\n", " ", (p->requestedby == NULL) ? "SYSTEM" : p->requestedby);

		if (p->installed_packages != NULL) {
			LINE1(length_longest_package + length_longest_old_rev + 1);
			fprintf(stdout, ESCAPE_BOLD "%-*s %-s" ESCAPE_ALL_ATTRIBUTES_OFF "\n", (int) length_longest_package, "INSTALLED PACKAGE", "REVISION");
			LINE1(length_longest_package + length_longest_old_rev + 1);
			apt_package *pp = p->installed_packages;
			while (pp != NULL) {
				fprintf(stdout, "%-*s %-s\n", (int) length_longest_package, pp->name, pp->installed_version);
				pp = pp->next_package;
			}
		}
		if (p->upgraded_packages != NULL) {
			LINE1(length_longest_package + length_longest_old_rev + length_longest_new_rev + 2);
			fprintf(stdout, ESCAPE_BOLD "%-*s %-*s %-s" ESCAPE_ALL_ATTRIBUTES_OFF "\n", (int) length_longest_package, "UPGRADED PACKAGE", (int) length_longest_old_rev, "INITIAL REVISION", "NEW REVISION");
			LINE1(length_longest_package + length_longest_old_rev + length_longest_new_rev + 2);
			apt_package *pp = p->upgraded_packages;
			while (pp != NULL) {
				fprintf(stdout, "%-*s %-*s %-s\n", (int) length_longest_package, pp->name, (int) length_longest_old_rev, pp->installed_version, pp->new_version);
				pp = pp->next_package;
			}
		}
		if (p->removed_packages != NULL) {
			LINE1(length_longest_package + length_longest_old_rev + 1);
			fprintf(stdout, ESCAPE_BOLD "%-*s %-s" ESCAPE_ALL_ATTRIBUTES_OFF "\n", (int) length_longest_package, "REMOVED PACKAGE", "REVISION");
			LINE1(length_longest_package + length_longest_old_rev + 1);
			apt_package *pp = p->removed_packages;
			while (pp != NULL) {
				fprintf(stdout, "%-*s %-s\n", (int) length_longest_package, pp->name, pp->installed_version);
				pp = pp->next_package;
			}
		}
		fprintf(stdout, "\n");
		p = p->previous_session;
		if (firstOnly) break;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void displayPacked( bool firstOnly ) {
	apt_session *p = session;
	while (p != NULL) {
		if (p->installed_packages != NULL) {
			apt_package *pp = p->installed_packages;
			while (pp != NULL) {
				fprintf(stdout, "%s/%s/%s/%s/INSTALL/%s/%s\n", p->start_date, p->start_time, p->end_time,(p->requestedby == NULL) ? "SYSTEM" : p->requestedby, pp->name, pp->installed_version);
				pp = pp->next_package;
			}
		}
		if (p->upgraded_packages != NULL) {
			apt_package *pp = p->upgraded_packages;
			while (pp != NULL) {
				fprintf(stdout, "%s/%s/%s/%s/UPGRADE/%s/%s/%s\n", p->start_date, p->start_time, p->end_time,(p->requestedby == NULL) ? "SYSTEM" : p->requestedby, pp->name, pp->installed_version, pp->new_version);
				pp = pp->next_package;
			}
		}
		if (p->removed_packages != NULL) {
			apt_package *pp = p->removed_packages;
			while (pp != NULL) {
				fprintf(stdout, "%s/%s/%s/%s/REMOVE/%s/%s\n", p->start_date, p->start_time, p->end_time,(p->requestedby == NULL) ? "SYSTEM" : p->requestedby, pp->name, pp->installed_version);
				pp = pp->next_package;
			}
		}
		p = p->previous_session;
		if (firstOnly) break;
	}
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void displayAptLog( bool firstOnly, bool packed ) {
	if (packed)
		displayPacked(firstOnly);
	else
		displayStandard(firstOnly);
}
//------------------------------------------------------------------------------
