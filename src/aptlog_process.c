//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: aptlog
// Friendly display of apt logs (Ubuntu-like system update)
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "defines.h"
#include "aptlog_process.h"
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern bool 		verboseMode;
extern apt_session 	*session;
extern char 		*buffer;
//------------------------------------------------------------------------------
// EXPORTED VARIABLE
//------------------------------------------------------------------------------
size_t	length_longest_package;
size_t	length_longest_old_rev;
size_t	length_longest_new_rev;
char 	*ptlog;
int 	linenumber;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void cleanPackage( apt_package *ppack ) {
	apt_package *p = ppack;
	while (p != NULL) {
		if (p->name != NULL) free(p->name);
		if (p->installed_version != NULL) free(p->installed_version);
		if (p->new_version != NULL) free(p->new_version);
		apt_package *temp = p->next_package;
		free(p);
		p = temp;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static apt_package *allocatePackage( char *pp, char *po, char *pn ) {
	apt_package *p = malloc(sizeof(apt_package));
	p->name = malloc(strlen(pp) + 1);
	strcpy(p->name, pp);
	p->installed_version = malloc(strlen(po) + 1);
	strcpy(p->installed_version, po);
	if (pn != NULL) {
		p->new_version = malloc(strlen(pn) + 1);
		strcpy(p->new_version, pn);
	} else
		p->new_version = NULL;
	p->next_package = NULL;
	return p;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static apt_session *allocateSession( void ) {
	apt_session *p = malloc(sizeof(apt_session));
	p->installed_packages = NULL;
	p->upgraded_packages = NULL;
	p->removed_packages = NULL;
	p->previous_session = NULL;
	p->next_session = NULL;
	p->start_date = NULL;
	p->start_time = NULL;
	p->command = NULL;
	p->requestedby = NULL;
	p->end_date = NULL;
	p->end_time = NULL;
	return p;
}
//------------------------------------------------------------------------------
static void readEntireTextFile( char *logfile ) {
	buffer = NULL;
	FILE *fp = fopen(logfile, "r");
	if (fp == NULL) {
		ERROR("cannot open apt log file ""%s""", logfile);
		return;
	}
	if (fseeko(fp, 0, SEEK_END) != 0) {
		ERROR("cannot set the position indicator of apt log file ""%s"" to end", logfile);
		fclose(fp);
		return;
	}
	off_t string_size = ftello(fp);
	if (string_size == -1) {
		ERROR("cannot read the position indicator of apt log file ""%s""", logfile);
		fclose(fp);
		return;
	}
	rewind(fp);
	buffer = (char *) calloc((size_t)string_size + 1, sizeof(char));
	size_t read_size = fread(buffer, sizeof(char), (size_t)string_size, fp);
	fclose(fp);
	if ((size_t)string_size != read_size) {
		ERROR("something went wrong when reading apt log file ""%s""", logfile);
		free(buffer);
		buffer = NULL;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *getItem( char *key ) {
	if (ptlog == NULL) return NULL;
	while (*ptlog == ' ') ++ptlog;
	char *pc;
	switch (*ptlog) {
	case '\0':
		return NULL;
	case '\n':
		*key = '\0';
		break;
	default:
		pc = ptlog;
		while (*ptlog != ':') ++ptlog;
		*ptlog = '\0';
		strncpy(key, pc, MAX_KEYWORD_LEN);
		break;
	}
	++ptlog;
	return key;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *getString( void ) {
	if (ptlog == NULL) return NULL;
	while (*ptlog == ' ') ++ptlog;
	if (*ptlog == '\0') return NULL;
	char *pc = ptlog;
	while (*ptlog != '\n') ++ptlog;
	*ptlog = '\0';
	++ptlog;
	char *res = malloc(strlen(pc) + 1);
	strcpy(res, pc);
	return res;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *getDate( void ) {
	if (ptlog == NULL) return NULL;
	while (*ptlog == ' ') ++ptlog;
	if (*ptlog == '\0') return NULL;
	char *pc = ptlog;
	while (*ptlog != ' ') ++ptlog;
	*ptlog = '\0';
	++ptlog;
	char *res = malloc(strlen(pc) + 1);
	strcpy(res, pc);
	return res;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static apt_package *getPackages( void ) {
	if (ptlog == NULL) return NULL;
	apt_package *firstpack = NULL;
	apt_package *pack = NULL;
	apt_package *previouspack = NULL;
	while (true) {
		while (*ptlog == ' ') ++ptlog;
		char *pp = ptlog;
		while (*ptlog != ' ' && *ptlog != '\n' && *ptlog != '\0') ++ptlog;
		if (*ptlog == '\n' || *ptlog == '\0') break;
		*ptlog = '\0';
		length_longest_package = MAX(length_longest_package, strlen(pp));

		++ptlog;
		while (*ptlog == ' ') ++ptlog;
		if (*ptlog != '(') {
			ERROR("Wrong syntax in apt log file, line %d (#1). Character expected: '(', obtained '%c'", linenumber, *ptlog);
			cleanPackage(firstpack);
			return NULL;
		}
		++ptlog;
		char *po = ptlog;
		while (*ptlog != ',' && *ptlog != ')' && *ptlog != '\n' && *ptlog != '\0') ++ptlog;
		if (*ptlog != ',' && *ptlog != ')') {
			ERROR("Wrong syntax in apt log file, line %d (#2). Character expected: ',' or ')', obtained '%c'", linenumber, *ptlog);
			cleanPackage(firstpack);
			return NULL;
		}
		char c = *ptlog;
		*ptlog = '\0';
		length_longest_old_rev = MAX(length_longest_old_rev, strlen(po));

		if (c == ',') {	// initial revision, new revision
			ptlog += 2;
			char *pn = ptlog;
			while (*ptlog != ')' && *ptlog != '\n' && *ptlog != '\0') ++ptlog;
			if (*ptlog != ')') {
				ERROR("Wrong syntax in apt log file, line %d (#3). Character expected: ')', obtained '%c'", linenumber, *ptlog);
				cleanPackage(firstpack);
				return NULL;
			}
			*ptlog = '\0';
			length_longest_new_rev = MAX(length_longest_new_rev, strlen(pn));

			++ptlog;
			pack = allocatePackage(pp, po, pn);
			if (pack == NULL) {
				cleanPackage(firstpack);
				return NULL;
			}
		} else {	// existing revision
			++ptlog;
			pack = allocatePackage(pp, po, NULL);
			if (pack == NULL) {
				cleanPackage(firstpack);
				return NULL;
			}
		}
		if (firstpack == NULL) firstpack = pack;
		if (previouspack != NULL) previouspack->next_package = pack;
		previouspack = pack;

		if (*ptlog == ',') ptlog +=2;
	}
	++ptlog;
	return firstpack;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void cleanAptLog( void ) {
	apt_session *p = session;
	while (p != NULL) {
		cleanPackage(p->installed_packages);
		cleanPackage(p->upgraded_packages);
		cleanPackage(p->removed_packages);
		if (p->start_date != NULL) free(p->start_date);
		if (p->start_time != NULL) free(p->start_time);
		if (p->command != NULL) free(p->command);
		if (p->requestedby != NULL) free(p->requestedby);
		if (p->end_date != NULL) free(p->end_date);
		if (p->end_time != NULL) free(p->end_time);
		apt_session *temp = p->previous_session;
		free(p);
		p = temp;
	}
	session = NULL;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void processAptLog( char *logfile ) {
	session = NULL;
	VERBOSE("%s", "Reading entire file in memory...");
	readEntireTextFile(logfile);	// Result in global variable buffer
	if (buffer == NULL) return;
	VERBOSE("%s", "Scanning data...");
	length_longest_package = 0;
	length_longest_old_rev = 0;
	length_longest_new_rev = 0;
	ptlog = buffer;
	apt_session *sessionPrevious = NULL;
	char keyword[MAX_KEYWORD_LEN + 1];
	linenumber = 0;
	while (true) {
		if (getItem(keyword) == NULL) break;
		++linenumber;
		if (strlen(keyword) == 0) continue;
		//VERBOSE("\tLine %d - Got keyword '%s'", linenumber, keyword);
		if (strcmp(keyword, KEY_STARTDATE) == 0) {
			sessionPrevious = session;
			session = allocateSession();
			if (session == NULL) {
				session = sessionPrevious;
				cleanAptLog();
				break;
			}
			if (sessionPrevious != NULL) {
				sessionPrevious->next_session = session;
				session->previous_session = sessionPrevious;
			}
			session->start_date = getDate();
			session->start_time = getString();
		} else {
			if (strcmp(keyword, KEY_COMMAND) == 0) {
				session->command = getString();
			} else {
				if (strcmp(keyword, KEY_REQUESTED) == 0) {
					session->requestedby = getString();
				} else {
					if (strcmp(keyword, KEY_INSTALL) == 0) {
						session->installed_packages = getPackages();
					} else {
						if (strcmp(keyword, KEY_UPGRADE) == 0) {
							session->upgraded_packages = getPackages();
						} else {
							if (strcmp(keyword, KEY_REMOVE) == 0) {
								session->removed_packages = getPackages();
							} else {
								if (strcmp(keyword, KEY_ENDDATE) == 0) {
									session->end_date = getDate();
									session->end_time = getString();
								} else {
									ERROR("unknown kewword '%s' in apt log file, line %d", keyword, linenumber);
									ptlog = NULL;
									break;
								}
							}
						}
					}
				}
			}
		}
		if (ptlog == NULL) {
			cleanAptLog();
			break;
		}
	}
	free(buffer);
	buffer = NULL;
}
//------------------------------------------------------------------------------
