//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: aptlog
// Friendly display of apt logs (Ubuntu-like system update)
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// ** IMPORTANT NOTE **
// This application is built on top of the libAlloc library for everything related
// to allocating and freeing memory space. This library is responsible for testing
// the results of the allocation primitives. For this reason, no test on the resul
// of an allocation is performed in these sources.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "defines.h"
#include "aptlog_process.h"
#include "aptlog_display.h"
#include "myalloc.h"
#include "aptlog_cmdline.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define APT_LOG_DIRECTORY				"/var/log/apt/"
#define APT_TMP_DIRECTORY				"/tmp/"

#define APT_LOG_CURRENT_FILENAME		"history.log"
#define APT_LOG_TMP_TEMPLATE			"history.log.%d"
#define APT_LOG_GZIP_TEMPLATE			"history.log.%d.gz"

#define MAX_LOGFILENAME					strlen(APT_LOG_DIRECTORY) + strlen(APT_LOG_CURRENT_FILENAME) + 5	// 5 = 1 (".") + 3 (1..999) + 1 ("\0")
#define MAX_ZIPFILENAME					strlen(APT_LOG_DIRECTORY) + strlen(APT_LOG_TMP_TEMPLATE) + 8		// 8 = 1 (".") + 3 (1..999) + 3 (".gz") + 1 ("\0")
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
static char 						*aptlogfile = NULL;
static char 						*aptzipfile	= NULL;
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
// These 2 following variables are global to ease the interrupt management!!!!
apt_session 	*session 	= NULL;
char 			*buffer		= NULL;
bool			verboseMode = false;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void cleanContext( int report ) {
	if (aptlogfile != NULL) free(aptlogfile);
	if (aptzipfile != NULL) free(aptzipfile);
	if (buffer != NULL) free(buffer);
	cleanAptLog();
	if (args_info.alloc_given) myallocreport(args_info.alloc_arg == alloc_arg_full);
	cmdline_parser_aptlog_free(&args_info);
	exit(report);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool existFile( char *file ) {
	struct stat locstat;
	if (file == NULL) return false;
	if (stat(file, &locstat) < 0) return false;
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void unzipFile( char *zipfile, char *logfile ) {
	size_t n = strlen(zipfile) + strlen(logfile) + 20;
	char *command = malloc(n * sizeof(char));
	snprintf(command, n, "gunzip -c %s > %s", zipfile, logfile);
	//VERBOSE("Command: %s", command);
	int unused = system(command);
	free(command);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void interrupt( int sig ) {
	INTERRUPT();
	cleanContext(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void myalloc_exit( int report ) {
	cleanContext(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_aptlog(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	verboseMode = args_info.verbose_given == 1;
	//---- Signals -------------------------------------------------------------
	signal(SIGABRT, interrupt);
	signal(SIGTERM, interrupt);
	signal(SIGINT, interrupt);
	//----  Go on --------------------------------------------------------------
	aptlogfile = malloc(MAX_LOGFILENAME);
	snprintf(aptlogfile, MAX_LOGFILENAME, "%s%s", APT_LOG_DIRECTORY, APT_LOG_CURRENT_FILENAME);
	VERBOSE("Processing %s...", aptlogfile);
	if (! existFile(aptlogfile)) {
		ERROR("apt log file ""%s"" does not exist", aptlogfile);
		goto EXIT;
	}
	processAptLog(aptlogfile);
//	if (session == NULL) goto EXIT;
//	displayAptLog(args_info.lastonly_given == 1, args_info.packed_given == 1);
//	cleanAptLog();
	if (session != NULL) {
		displayAptLog(args_info.lastonly_given == 1, args_info.packed_given == 1);
		cleanAptLog();
	}

	if (! args_info.lastonly_given) {
		aptzipfile = malloc(MAX_ZIPFILENAME);
		int idx = 1;
		while (true) {
			snprintf(aptzipfile, MAX_ZIPFILENAME, APT_LOG_DIRECTORY APT_LOG_GZIP_TEMPLATE, idx);
			if (! existFile(aptzipfile)) break;
			snprintf(aptlogfile, MAX_LOGFILENAME, APT_TMP_DIRECTORY APT_LOG_TMP_TEMPLATE, idx);
			VERBOSE("Unzipping %s...", aptzipfile);
			unzipFile(aptzipfile, aptlogfile);
			VERBOSE("Processing %s...", aptlogfile);
			if (! existFile(aptlogfile)) {
				ERROR("apt log file ""%s"" does not exist", aptlogfile);
				break;
			}
			processAptLog(aptlogfile);
			unlink(aptlogfile);
			if (session == NULL) break;
			displayAptLog(false, args_info.packed_given == 1);
			cleanAptLog();
			++idx;
			if (idx == 1000) break;
		}
	}
	//---- Exit ----------------------------------------------------------------
	cleanContext(EXIT_SUCCESS);
EXIT:
	cleanContext(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
