 # *aptlog*, friendly display of apt logs (Ubuntu-like system update).

**aptlog** displays, in a way I find more readable and visually usable, the logs of the *apt* package management command. These logs are stored in the system directory */var/log/apt/* as a text file (recent commands) and gzipped (old commands). With the *--packed* option, **aptlog** displays the logs in a form easy for "grep" to find, for example, anything relating to a given package.

## LICENSE
**aptlog** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ aptlog --help
Usage: aptlog [OPTION]...
Friendly display of apt logs (Ubuntu-like system update)

  -h, --help        Print help and exit
      --full-help   Print help, including hidden options, and exit
  -V, --version     Print version and exit
  -l, --lastonly    Display only the last apt action.  (default=off)
  -p, --packed      Display information in basic form for grep command
                      (default=off)
  -v, --verbose     Verbose mode.  (default=off)

Exit: returns a non-zero status if an error is detected.

$ aptlog --version
aptlog - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
aptlog - Version 1.0.0
$ aptlog --lastonly
2020-12-11 - 08:39:27 / 08:39:31. Command: aptdaemon role='role-commit-packages' sender=':1.530'
                                  Requested by: SYSTEM
────────────────────────────────────────────────────────────────────────────────────────────────────────────────
UPGRADED PACKAGE                                 INITIAL REVISION            NEW REVISION
────────────────────────────────────────────────────────────────────────────────────────────────────────────────
libparted2:amd64                                 3.3-4                       3.3-4ubuntu0.20.10.1
libparted-fs-resize0:amd64                       3.3-4                       3.3-4ubuntu0.20.10.1
parted:amd64                                     3.3-4                       3.3-4ubuntu0.20.10.1

$ aptlog -lp
2020-12-11/08:39:27/08:39:31/SYSTEM/UPGRADE/libparted2:amd64/3.3-4/3.3-4ubuntu0.20.10.1
2020-12-11/08:39:27/08:39:31/SYSTEM/UPGRADE/libparted-fs-resize0:amd64/3.3-4/3.3-4ubuntu0.20.10.1
2020-12-11/08:39:27/08:39:31/SYSTEM/UPGRADE/parted:amd64/3.3-4/3.3-4ubuntu0.20.10.1
$ 

```
## STRUCTURE OF THE APPLICATION
This section walks you through **aptlog**'s structure. Once you understand this structure, you will easily find your way around in **aptlog**'s code base.

``` bash
$ yaTree
./                          # Application level
├── README_images/          # Images for documentation
│   └── aptlog-01.png       # 
├── src/                    # Source directory
│   ├── Makefile            # Makefile
│   ├── adapt_window_cols.c # Adapt numbers of columns of the window to a suitable number
│   ├── adapt_window_cols.h # Header file for column adapter
│   ├── aptlog.c            # Main program
│   ├── aptlog.ggo          # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   ├── aptlog_display.c    # Dislay apt log file
│   ├── aptlog_display.h    # Header file for display apt log file
│   ├── aptlog_process.c    # Process apt log file
│   ├── aptlog_process.h    # Header file for process apt log file
│   └── defines.h           # General header file
├── COPYING.md              # GNU General Public License markdown file
├── LICENSE.md              # License markdown file
├── Makefile                # Makefile
├── README.md               # ReadMe markdown file
├── RELEASENOTES.md         # Release Notes markdown file
└── VERSION                 # Version identification text file

2 directories, 17 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd aptlog
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd aptlog
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level). We consider that $BIN_DIR is a part of the PATH.
```

## NOTES

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.23.
- Developped and tested on XUBUNTU 20.10, GCC v10.2.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***